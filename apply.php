<?php

require_once("config.php");

if(isset($_POST['register'])){

    
    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    
    $password = password_hash($_POST["password"], PASSWORD_DEFAULT);
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);


    
    $sql = "INSERT INTO loan (period, reason, sector, pin) 
            VALUES (:(period, :reason, :sector, :pin)";
    $stmt = $db->prepare($sql);

    
    $params = array(
        ":period" => $period,
        ":reason" => $reason,
        ":sector" => $sector,
        ":pin" => $pin
    );

    
    $saved = $stmt->execute($params);

    
    
    if($saved) header("Location: apply.php");
}

?>
<html>
  <head>
 <!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/css/mdb.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="kava/css/bootstrap1.css" type="text/css">
    <link rel="stylesheet" href="kava/css/signup.css" type="text/css">
    
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans&display=swap" rel="stylesheet">
    <title>Apply | Kava</title>
  </head>
  <body>
    <nav id="navig">
      <a href="index.php"><img src="img/logo.jpg" class="logo" alt="logo"></a>
      <ul>
          <li><a href="index.php">HOME</a></li>
        <li><a href="login.php">LOG IN</a></li>
        <li><a href="signup.php">SIGN UP</a> </li>
        <li><a href="apply.php">Take Loan</a>
      
      </ul>
    </nav>
   
      </ul>
    </nav>

<div class="container b" >

<h2>View our plans</h2>
</div>
    <div class="row">
      <div class="col-md-06 ni">
<div class="card" style="width: 18rem;">
  <img src="https://cdn.pixabay.com/photo/2019/09/21/09/28/landscape-4493462_960_720.jpg" class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">Personal</h5>
    <p class="card-text">Here We offer</p>
    <a href="#" class="btn btn-primary">Check Out</a>
  </div>
</div>
</div>
<div class="col-md-06 ni">
<div class="card" style="width: 18rem;">
  <img src="https://cdn.pixabay.com/photo/2019/09/21/09/28/landscape-4493462_960_720.jpg" class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">Business</h5>
    <p class="card-text">Here We offer </p>
    <a href="#" class="btn btn-primary">Check Out</a>
  </div>
</div>
</div>
<div class="col-md-06 ni">
<div class="card" style="width: 18rem;">
  <img src="https://cdn.pixabay.com/photo/2019/09/21/09/28/landscape-4493462_960_720.jpg" class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">Health</h5>
    <p class="card-text">Here We offer</p>
    <a href="#" class="btn btn-primary">Check Out</a>
  </div>
</div>
</div>
<div class="col-md-06 ni">
<div class="card" style="width: 18rem;">
  <img src="https://cdn.pixabay.com/photo/2019/09/21/09/28/landscape-4493462_960_720.jpg" class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">Education</h5>
    <p class="card-text">Here We offer</p>
    <a href="#" class="btn btn-primary">Check Out</a>
  </div>
</div></div></div>
  </body>
</html>
git remote add origin https://gitlab.com/marvzach/kava-loans-assestment.git